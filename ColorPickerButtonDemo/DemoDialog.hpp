#pragma once

#include "ColorPickerButton/ColorPickerButton.hpp"


class DemoDialog : public CDialogEx
{
#ifdef AFX_DESIGN_TIME
   enum { IDD = IDD_COLORPICKERBUTTONDEMO_DIALOG };
#endif

public:
   DemoDialog(CWnd* pParent = nullptr);

protected:
   virtual BOOL OnInitDialog();
   virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

   DECLARE_MESSAGE_MAP()
   afx_msg void OnPaint();
   afx_msg HCURSOR OnQueryDragIcon();
   afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
   afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
   afx_msg void OnColorPickerChanged(NMHDR* pNMHDR, LRESULT* pResult);
   afx_msg void OnClickedEnabled();

private:
   HICON					m_hIcon;
   ColorPickerButton m_btnColorPicker;
   CStatic           m_lblColorSwatch;
   CButton				m_chkAutomatic;
   CButton           m_chkEnabled;
};
