#include "PCH.hpp"
#include "Framework.hpp"
#include "DemoDialog.hpp"
#include "DemoApp.hpp"
#include "Resources.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


DemoDialog::DemoDialog(CWnd* pParent /* = nullptr */)
   : CDialogEx(IDD_COLORPICKERBUTTONDEMO_DIALOG, pParent)
   , m_hIcon(::AfxGetApp()->LoadIcon(IDR_MAINFRAME))
{ }

BOOL DemoDialog::OnInitDialog()
{
   CDialogEx::OnInitDialog();

   // Set the icon for this dialog.
   SetIcon(m_hIcon, TRUE);	  // set big icon
   SetIcon(m_hIcon, FALSE);  // set small icon

   m_chkEnabled  .SetCheck(BST_CHECKED);
   m_chkAutomatic.SetCheck(BST_CHECKED);

   const auto clrHighlight = ::GetSysColor(COLOR_HIGHLIGHT);
   m_btnColorPicker.SetDefaultColor(clrHighlight);

   _ASSERTE(m_btnColorPicker.GetColor() == CLR_DEFAULT);
#if 0
   m_btnColorPicker.SetColor(RGB(0, 255, 10));
#endif  // 0

   return TRUE;  // return TRUE  unless you set the focus to a control
}

void DemoDialog::DoDataExchange(CDataExchange* pDX)
{
   CDialogEx::DoDataExchange(pDX);
   DDX_Control(pDX, IDC_COLORPICKERBUTTON, m_btnColorPicker);
   DDX_Control(pDX, IDC_COLORSWATCH      , m_lblColorSwatch);
   DDX_Control(pDX, IDC_AUTOMATIC        , m_chkAutomatic);
   DDX_Control(pDX, IDC_ENABLED          , m_chkEnabled);
}

BEGIN_MESSAGE_MAP(DemoDialog, CDialogEx)
   ON_WM_PAINT()
   ON_WM_QUERYDRAGICON()
   ON_WM_GETMINMAXINFO()
   ON_NOTIFY(CPBN_SELCHANGED, IDC_COLORPICKERBUTTON, &DemoDialog::OnColorPickerChanged)
   ON_WM_CTLCOLOR()
   ON_BN_CLICKED(IDC_ENABLED, &DemoDialog::OnClickedEnabled)
END_MESSAGE_MAP()

void DemoDialog::OnPaint()
{
   if (IsIconic())
   {
      CPaintDC dc(this); // device context for painting

      this->SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

      // Draw icon centered in client rectangle.
      const auto cxIcon = ::GetSystemMetrics(SM_CXICON);
      const auto cyIcon = ::GetSystemMetrics(SM_CYICON);
      CRect rc;
      GetClientRect(&rc);
      dc.DrawIcon((rc.Width () - cxIcon + 1) / 2,
                  (rc.Height() - cyIcon + 1) / 2,
                  m_hIcon);
   }
   else
   {
      CDialogEx::OnPaint();
   }
}

HCURSOR DemoDialog::OnQueryDragIcon()
{
   return static_cast<HCURSOR>(m_hIcon);
}

void DemoDialog::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
   lpMMI->ptMinTrackSize.x = 192;
   lpMMI->ptMinTrackSize.y = 140;
   CDialogEx::OnGetMinMaxInfo(lpMMI);
}

HBRUSH DemoDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
   if (pWnd == &m_lblColorSwatch)
   {
      pDC->SetDCBrushColor(this->m_btnColorPicker.GetColorAsRGB());
      return static_cast<HBRUSH>(::GetStockObject(DC_BRUSH));
   }
   else
   {
      return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
   }
}

void DemoDialog::OnColorPickerChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
   _ASSERTE(pNMHDR);
   const auto pNMCPB = reinterpret_cast<const NMCOLORPICKERBUTTON*>(pNMHDR);
   _ASSERTE(pNMCPB);
   _ASSERTE(pNMCPB->hdr.code == CPBN_SELCHANGED);
   _ASSERTE(pNMCPB->hdr.idFrom == IDC_COLORPICKERBUTTON);
   _ASSERTE(pNMCPB->hdr.hwndFrom == m_btnColorPicker.GetSafeHwnd());
   _ASSERTE(pNMCPB->clrCurrent == m_btnColorPicker.GetColor());

   m_chkAutomatic.SetCheck((pNMCPB->clrCurrent == CLR_DEFAULT) ? BST_CHECKED
                                                               : BST_UNCHECKED);

   m_lblColorSwatch.Invalidate(FALSE);

   *pResult = 0;
}

void DemoDialog::OnClickedEnabled()
{
   const auto enabled = (m_chkEnabled.GetCheck() == BST_CHECKED);
   m_btnColorPicker.EnableWindow(enabled ? TRUE : FALSE);
   m_btnColorPicker.Invalidate(TRUE);
}
