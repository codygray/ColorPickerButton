#pragma once


HMODULE SafeLoadSystemLibrary(const TCHAR* pszLibraryName);

FARPROC SafeGetProcAddressInKernel(const char* pszProcName);
FARPROC SafeGetProcAddressInUser  (const char* pszProcName);

bool IsWinXPOrLater() noexcept;

CRect GetScreenRect(HWND hWnd) noexcept;
