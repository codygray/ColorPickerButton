#include "PCH.hpp"
#include "ColorPickerDialog.hpp"
#include "DownlevelHelper.hpp"
#include <ShellScalingApi.h>


//////////////////////////////////////////////////
// ColorPickerDialog
//////////////////////////////////////////////////

ColorPickerDialog::ColorPickerDialog(COLORREF clrInitial, CWnd* pParentWnd, HPALETTE hPalette)
   : CMFCColorDialog(clrInitial, 0, pParentWnd, hPalette)
   , m_wndScreen    (*this)
{ }


BOOL ColorPickerDialog::OnInitDialog()
{
   const auto result = CMFCColorDialog::OnInitDialog();

   // I wish we could find the window corresponding to the label of the "Luminance" edit control
   // (IDC_AFXBARRES_L) and correct its spelling (the text in the MFC-provided dialog template is
   // "&Luminence:"; note the substitution of an "a" for an "e"). Unfortunately, this control is
   // nested two sub-dialogs deep, and neither of those sub-dialogs have been created at this point.
   // (The m_pColourSheetTwo member variable contains a pointer to the C++ class instance that is
   // newed up by this base class's constructor, but the underlying window hasn't been created yet.)
   // This parent dialog gets a WM_PARENTNOTIFY message upon the creation of the first sub-dialog,
   // but that dialog's window would then need to be subclassed in order to receive a notification
   // when its sub-dialog is created. Gosh, wouldn't it be easier for Microsoft to just fix this bug
   // in the resource file? I don't even know how this happened, since the word is spelled correctly
   // throughout the source code, and one would think that the resource file(s) contents, which are
   // displayed directly to end-users, would be even more diligently composed and checked for errors
   // than the source code files seen only by developers who see fit to dig into them.

   // For similar reasons, it is ill-advised to try to set all descendant windows/controls to use
   // the dialog font (e.g., by calling this->GetFont(), and then sending that font's handle to all
   // descendants with this->SendMessageToDescendants()). The child dialog representing the second
   // tab has not been created at this point, so would not receive the message and would thus use a
   // different font than the child dialog representing the first tab.

   // If we failed to build our full-screen color picker window, disable and hide the button
   // that would invoke it.
   if (m_wndScreen.GetSafeHwnd() == NULL)
   {
      m_btnColorSelect.EnableWindow(FALSE);
      m_btnColorSelect.ShowWindow(SW_HIDE);
   }

   return result;
}

LRESULT ColorPickerDialog::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
   // Override the base class (CMFCColorDialog) handling of click events on the "Select" button
   // in the dialog to display our own color-picker implementation.
   if ((message == WM_COMMAND) && (LOWORD(wParam) == IDC_AFXBARRES_COLOR_SELECT))
   {
      _ASSERTE(reinterpret_cast<HWND>(lParam) ==
               this->GetDlgItem(IDC_AFXBARRES_COLOR_SELECT)->GetSafeHwnd());
      _ASSERTE(!m_bPickerMode);

      m_wndScreen.Start(m_NewColor);

      return 0;
   }
   return CMFCColorDialog::WindowProc(message, wParam, lParam);
}

//////////////////////////////////////////////////
// FullScreenColorPickerWindow
//////////////////////////////////////////////////

FullScreenColorPickerWindow::FullScreenColorPickerWindow(ColorPickerDialog& dlg)
   : CWnd                    ()
   , m_dlg                   (dlg)
   , m_dcScreen              (nullptr)
   , m_pfnLogicalToPhysicalPt(reinterpret_cast<decltype(m_pfnLogicalToPhysicalPt)>(SafeGetProcAddressInUser(_CRT_STRINGIZE(LogicalToPhysicalPointForPerMonitorDPI))))
   , m_clrOriginal           (RGB(0, 0, 0))
{
   HCURSOR    hCursor = NULL;
   const auto pApp    = AfxGetApp();
   _ASSERTE(pApp);
   if (pApp)
   {
      hCursor = pApp->LoadCursor(IDC_AFXBARRES_COLOR);
   }
   if (!hCursor)
   {
      hCursor = ::LoadCursor(NULL, IDC_CROSS);
   }
   _ASSERTE(hCursor);

   VERIFY(this->CreateEx(WS_EX_TOPMOST | WS_EX_TOOLWINDOW,
                         ::AfxRegisterWndClass(CS_PARENTDC | CS_SAVEBITS,
                                               hCursor,
                                               NULL,
                                               NULL),
                         TEXT(""),
                         WS_POPUP,
                         ::GetSystemMetrics(SM_XVIRTUALSCREEN),
                         ::GetSystemMetrics(SM_YVIRTUALSCREEN),
                         ::GetSystemMetrics(SM_CXVIRTUALSCREEN),
                         ::GetSystemMetrics(SM_CYVIRTUALSCREEN),
                         dlg.GetSafeHwnd(),
                         NULL,
                         nullptr));
}


FullScreenColorPickerWindow::~FullScreenColorPickerWindow()
{
   if (this->GetSafeHwnd())
   {
      VERIFY(this->DestroyWindow());
   }
}


void FullScreenColorPickerWindow::Start(COLORREF clrInitial)
{
   _ASSERTE(this->GetSafeHwnd() != NULL);

   m_clrOriginal       = clrInitial;
   m_dlg.m_bPickerMode = TRUE;
   VERIFY(this->ShowWindow(SW_SHOW) == SW_HIDE);
}

void FullScreenColorPickerWindow::Stop(bool confirm /* = true */)
{
   _ASSERTE(this->GetSafeHwnd() != NULL);

   m_dlg.m_bPickerMode = FALSE;
   VERIFY(m_dlg.m_wndScreen.ShowWindow(SW_HIDE) != SW_HIDE);

   if (!confirm)
   {
      m_dlg.SetNewColor(m_clrOriginal);
   }

   const auto clr    = m_dlg.GetColor();
   const auto rColor = GetRValue(clr);
   const auto gColor = GetGValue(clr);
   const auto bColor = GetBValue(clr);
   m_dlg.SetPageOne(rColor, gColor, bColor);
   m_dlg.SetPageTwo(rColor, gColor, bColor);
}


BEGIN_MESSAGE_MAP(FullScreenColorPickerWindow, CWnd)
   ON_WM_MOUSEMOVE()
   ON_WM_LBUTTONDOWN()
   ON_WM_RBUTTONDOWN()
   ON_WM_MBUTTONDOWN()
   ON_WM_LBUTTONUP()
   ON_WM_RBUTTONUP()
   ON_WM_MBUTTONUP()
   ON_WM_KEYDOWN()
END_MESSAGE_MAP()

void FullScreenColorPickerWindow::OnMouseMove(UINT nFlags, CPoint point)
{
   CWnd::OnMouseMove(nFlags, point);

   // Get the point corresponding to the current location of the mouse pointer,
   // and convert that point from client coordinates into screen coordinates.
   this->ClientToScreen(&point);

   // If the underlying Windows version supports per-monitor DPI, then we will have retrieved
   // a non-null pointer to a conversion function that converts the logical screen coordinates
   // to physical screen coordinates. This will remove the effects of any DPI virtualization
   // that may be applied to us.
   if (m_pfnLogicalToPhysicalPt)
   {
      VERIFY(m_pfnLogicalToPhysicalPt(NULL, &point));
   }

   // Retrieve the color of the pixel at the corresponding location from the screen DC,
   // and update the color in the dialog.
   const auto clr = m_dcScreen.GetPixel(point);
   _ASSERTE(clr != CLR_INVALID);
   m_dlg.SetNewColor(clr);
}

void FullScreenColorPickerWindow::OnLButtonDown(UINT nFlags, CPoint point)
{
   CWnd::OnLButtonDown(nFlags, point);

   this->Stop(true);
}

void FullScreenColorPickerWindow::OnRButtonDown(UINT nFlags, CPoint point)
{
   CWnd::OnRButtonDown(nFlags, point);

   this->Stop(true);
}

void FullScreenColorPickerWindow::OnMButtonDown(UINT nFlags, CPoint point)
{
   CWnd::OnMButtonDown(nFlags, point);

   this->Stop(true);
}

void FullScreenColorPickerWindow::OnLButtonUp(UINT nFlags, CPoint point)
{
   CWnd::OnLButtonUp(nFlags, point);

   this->Stop(true);
}

void FullScreenColorPickerWindow::OnRButtonUp(UINT nFlags, CPoint point)
{
   CWnd::OnRButtonUp(nFlags, point);

   this->Stop(true);
}

void FullScreenColorPickerWindow::OnMButtonUp(UINT nFlags, CPoint point)
{
   CWnd::OnMButtonUp(nFlags, point);

   this->Stop(true);
}

void FullScreenColorPickerWindow::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
   CWnd::OnKeyDown(nChar, nRepCnt, nFlags);

   switch (nChar)
   {
      case VK_RETURN:
      {
         this->Stop(true);
         break;
      }
      case VK_ESCAPE:
      {
         this->Stop(false);
         break;
      }
   }
}
