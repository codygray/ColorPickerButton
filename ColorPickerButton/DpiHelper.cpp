#include "PCH.hpp"
#include "DpiHelper.hpp"
#include "DownlevelHelper.hpp"
#include <ShellScalingApi.h>


namespace {

constexpr UINT kReferenceDpi = 96;

UINT GetDpiForWindow(HWND hWnd) noexcept
{
   auto succeeded = false;
   auto result    = kReferenceDpi;

   if (hWnd)
   {
      typedef UINT (WINAPI * fnGetDpiForWindow)(HWND);
      const auto pfnGetDpiForWindow = reinterpret_cast<fnGetDpiForWindow>(SafeGetProcAddressInUser(_CRT_STRINGIZE(GetDpiForWindow)));
      if (pfnGetDpiForWindow)
      {
         result    = pfnGetDpiForWindow(hWnd);
         succeeded = true;
      }
   }
   else
   {
      typedef UINT (WINAPI * fnGetDpiForSystem)(void);
      const auto pfnGetDpiForSystem = reinterpret_cast<fnGetDpiForSystem>(SafeGetProcAddressInUser(_CRT_STRINGIZE(GetDpiForSystem)));
      if (pfnGetDpiForSystem)
      {
         result    = pfnGetDpiForSystem();
         succeeded = true;
      }
   }

   if (!succeeded)
   {
      const auto hlibShCore = SafeLoadSystemLibrary(TEXT("ShCore"));
      if (hlibShCore)
      {
         typedef HRESULT (WINAPI * fnGetDpiForMonitor)(HMONITOR, MONITOR_DPI_TYPE, UINT*, UINT*);
         const auto pfnGetDpiForMonitor = reinterpret_cast<fnGetDpiForMonitor>(::GetProcAddress(hlibShCore, _CRT_STRINGIZE(GetDpiForMonitor)));
         if (pfnGetDpiForMonitor)
         {
            const auto hMonitor = hWnd ? ::MonitorFromWindow(hWnd        , MONITOR_DEFAULTTOPRIMARY)
                                       : ::MonitorFromPoint (CPoint(0, 0), MONITOR_DEFAULTTOPRIMARY);
            _ASSERTE(hMonitor);

            UINT xDpi;
            UINT yDpi;
            const auto hr = pfnGetDpiForMonitor(hMonitor, MDT_EFFECTIVE_DPI, &xDpi, &yDpi);
            _ASSERTE(SUCCEEDED(hr));
            if (SUCCEEDED(hr))
            {
               _ASSERTE(xDpi == yDpi);
               result    = xDpi;
               succeeded = true;
            }
         }
         VERIFY(::FreeLibrary(hlibShCore));
      }
   }

   if (!succeeded)
   {
      const auto hdcScreen = ::GetDC(NULL);
      if (hdcScreen)
      {
         result    = ::GetDeviceCaps(hdcScreen, LOGPIXELSX);
         succeeded = true;

         VERIFY(::ReleaseDC(NULL, hdcScreen));
      }
   }

   return result;
}

}  // anonymous namespace

DpiHelper::DpiHelper(HWND hWnd) noexcept
   : m_dpi(GetDpiForWindow(hWnd))
{ }


bool DpiHelper::IsHighDpi() const noexcept
{
   return (this->Dpi() > kReferenceDpi);
}


UINT DpiHelper::Dpi() const noexcept
{
   return m_dpi;
}


UINT DpiHelper::Adjust(UINT value) const noexcept
{
   const auto result = ::MulDiv(value, this->Dpi(), kReferenceDpi);
   _ASSERTE(result >= 0);
   return static_cast<UINT>(result);
}

int DpiHelper::Adjust(int value) const noexcept
{
   _ASSERTE(m_dpi <= INT_MAX);
   return ::MulDiv(value, static_cast<int>(this->Dpi()), static_cast<int>(kReferenceDpi));
}
