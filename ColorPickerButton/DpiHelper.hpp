#pragma once


class DpiHelper
{
public:

   DpiHelper(HWND hWnd) noexcept;


   bool IsHighDpi() const noexcept;

   UINT Dpi() const noexcept;

   UINT Adjust(UINT value) const noexcept;
   int  Adjust(int  value) const noexcept;


private:
   UINT m_dpi;
};
