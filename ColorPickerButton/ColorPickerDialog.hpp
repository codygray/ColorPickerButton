#pragma once

#include <AfxColorDialog.h>


class ColorPickerDialog;


class FullScreenColorPickerWindow : public CWnd
{
public:

   explicit FullScreenColorPickerWindow(ColorPickerDialog& dlg);

   virtual ~FullScreenColorPickerWindow() override;

   void Start(COLORREF clrInitial);
   void Stop(bool confirm /* = true */);

protected:

   DECLARE_MESSAGE_MAP()
   afx_msg void OnMouseMove  (UINT nFlags, CPoint point);
   afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
   afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
   afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
   afx_msg void OnLButtonUp  (UINT nFlags, CPoint point);
   afx_msg void OnRButtonUp  (UINT nFlags, CPoint point);
   afx_msg void OnMButtonUp  (UINT nFlags, CPoint point);
   afx_msg void OnKeyDown    (UINT nChar, UINT nRepCnt, UINT nFlags);

private:

   ColorPickerDialog& m_dlg;
   CClientDC          m_dcScreen;
   BOOL     (WINAPI * m_pfnLogicalToPhysicalPt)(HWND, LPPOINT);
   COLORREF           m_clrOriginal;
};


class ColorPickerDialog : public CMFCColorDialog
{
   friend FullScreenColorPickerWindow;

public:
   explicit ColorPickerDialog(COLORREF clrInitial, CWnd* pParentWnd, HPALETTE hPalette);

protected:
   virtual BOOL    OnInitDialog() override;
   virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam) override;

private:
   FullScreenColorPickerWindow m_wndScreen;
};
