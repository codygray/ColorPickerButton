#include "PCH.hpp"
#include "DownlevelHelper.hpp"
#include <string>

#ifndef LOAD_LIBRARY_SEARCH_SYSTEM32
   #define LOAD_LIBRARY_SEARCH_SYSTEM32  0x00000800
#endif  // LOAD_LIBRARY_SEARCH_SYSTEM32


HMODULE SafeLoadSystemLibrary(const TCHAR* pszLibraryName)
{
   _ASSERTE(pszLibraryName != nullptr);
   const auto cchLibraryName = _tcslen(pszLibraryName);
   _ASSERTE(cchLibraryName > 0);

   HMODULE hlibResult = nullptr;

   // If supported, we want to specify the LOAD_LIBRARY_SEARCH_SYSTEM32 flag. This flag is only
   // supported when KB2533623 is installed on Windows Vista or 7, or on Windows 8 and later.
   // Since the same update that adds support for LOAD_LIBRARY_SEARCH_SYSTEM32 also adds the
   // SetDefaultDllDirectories() function, we can use the existence of that API as a proxy
   // to check support for the LOAD_LIBRARY_SEARCH_SYSTEM32 flag.
   const auto hlibKernel32 = ::GetModuleHandle(TEXT("kernel32"));
   _ASSERTE(hlibKernel32);  // should already be loaded!
   if (hlibKernel32)
   {
      const auto pfSetDefaultDllDirectories = ::GetProcAddress(hlibKernel32,
                                                               _CRT_STRINGIZE(SetDefaultDllDirectories));
      if (pfSetDefaultDllDirectories)
      {
         hlibResult = ::LoadLibraryEx(pszLibraryName, nullptr, LOAD_LIBRARY_SEARCH_SYSTEM32);
      }
      else
      {
         // When the LOAD_LIBRARY_SEARCH_SYSTEM32 flag is not supported, fall back
         // to using the next best thing: LOAD_WITH_ALTERED_SEARCH_PATH.
         std::basic_string<TCHAR> strPath(MAX_PATH, TEXT('\0'));
         const auto               cchPath = ::GetSystemDirectory(&strPath[0], MAX_PATH);
         if (cchPath && (cchPath < strPath.size()))
         {
            strPath.resize(cchPath);

            strPath.reserve(cchPath + cchLibraryName + 1);
            strPath.append(TEXT("\\"));
            strPath.append(pszLibraryName, cchLibraryName);

            hlibResult = ::LoadLibraryEx(strPath.c_str(), nullptr, LOAD_WITH_ALTERED_SEARCH_PATH);
         }
      }
   }

   return hlibResult;
}


namespace {

FARPROC SafeGetProcAddressInSystemLibrary(const TCHAR* pszLibraryName,
                                          const char * pszProcName)
{
   _ASSERTE(pszLibraryName != nullptr);
   _ASSERTE(pszProcName    != nullptr);
   _ASSERTE(_tcslen(pszLibraryName) > 0);
   _ASSERTE(strlen (pszProcName   ) > 0);

   const auto hLib = ::GetModuleHandle(pszLibraryName);
   _ASSERTE(hLib);  // this module should already be loaded!
   if (hLib)
   {
      return ::GetProcAddress(hLib, pszProcName);
   }
   else
   {
      return nullptr;
   }
}

}  // anonymous namespace

FARPROC SafeGetProcAddressInKernel(const char* pszProcName)
{
   return SafeGetProcAddressInSystemLibrary(TEXT("kernel32"), pszProcName);
}

FARPROC SafeGetProcAddressInUser(const char* pszProcName)
{
   return SafeGetProcAddressInSystemLibrary(TEXT("user32"), pszProcName);
}


bool IsWinXPOrLater() noexcept
{
   OSVERSIONINFO osvi;
   osvi.dwOSVersionInfoSize = sizeof(osvi);
   #pragma warning(suppress: 4996)  // suppress deprecation warning: it's OK if the API lies to us
   const auto succeeded = ::GetVersionEx(&osvi);
   _ASSERTE(succeeded);
   return (succeeded                                    &&
           (osvi.dwPlatformId == VER_PLATFORM_WIN32_NT) &&
           ((osvi.dwMajorVersion > 5)
            ||
            ((osvi.dwMajorVersion == 5) && (osvi.dwMinorVersion >= 1))));
}

CRect GetScreenRect(HWND hWnd) noexcept
{
   CRect rcResult(CPoint(0, 0),
                  CSize(::GetSystemMetrics(SM_CXSCREEN),
                        ::GetSystemMetrics(SM_CYSCREEN)));

   const auto hlibUser32 = ::GetModuleHandle(TEXT("user32"));
   _ASSERTE(hlibUser32);  // should already be loaded!
   if (hlibUser32)
   {
      typedef HMONITOR (WINAPI * fnMonitorFromWindow)(HWND, DWORD);
      typedef BOOL     (WINAPI * fnGetMonitorInfo)   (HMONITOR, LPMONITORINFO);
      const auto pfnMonitorFromWindow = reinterpret_cast<fnMonitorFromWindow>(::GetProcAddress(hlibUser32, _CRT_STRINGIZE(MonitorFromWindow)));
      const auto pfnGetMonitorInfo    = reinterpret_cast<fnGetMonitorInfo>   (::GetProcAddress(hlibUser32, _CRT_STRINGIZE(GetMonitorInfo)));
      if (pfnMonitorFromWindow && pfnGetMonitorInfo)
      {
         const auto hMonitor = pfnMonitorFromWindow(hWnd, MONITOR_DEFAULTTONEAREST);

         MONITORINFO mi;
         mi.cbSize = sizeof(mi);
         VERIFY(pfnGetMonitorInfo(hMonitor, &mi));

         rcResult = mi.rcWork;
      }
   }
   return rcResult;
}
